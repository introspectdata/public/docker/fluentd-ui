FROM ruby:2.6-stretch

RUN apt update
RUN apt -y upgrade
RUN apt install -y build-essential libssl-dev libxml2-dev libxslt1-dev ruby-dev ruby
RUN gem install fluentd-ui
RUN fluentd-ui setup
EXPOSE 80
ENTRYPOINT ["fluentd-ui", "start", "--daemonize"]
